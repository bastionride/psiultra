﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AccurateTelekenesis : MonoBehaviour {
	public GameObject _forceField;
	public Transform _look;
	public Transform _pullPoint;
	private int interactiveLayer, handleLayer;
	private float pullForce = 1000f;

	private bool usingForce = true;

	public List<HoldedObject> _holdedObjects = new List<HoldedObject>();

	void Start () {
		 interactiveLayer = LayerMask.NameToLayer("Interactive");
		 handleLayer = LayerMask.NameToLayer("Handle");
	}
	

	public void SetActive(bool active) {
		_forceField.SetActive(active);
		if (!active) {
			_holdedObjects.Clear();
		}
	}

    public void MoveObjects(float pull) {
        foreach (var obj in _holdedObjects) {
            obj.distance += pull;
        }
    }

	public void ThrowObjects () {
		for (int objIdx = 0; objIdx < _holdedObjects.Count; objIdx++) {
				Vector3 forceDirection = transform.position - _holdedObjects[objIdx].rb.transform.position;
				_holdedObjects[objIdx].rb.AddForce(_look.forward * 25f, ForceMode.Impulse);
			}
	}

	void FixedUpdate () {
		for (int objIdx = 0; objIdx < _holdedObjects.Count; objIdx++) {
			if (Vector3.Distance(_holdedObjects[objIdx].rb.transform.position, _pullPoint.position) > 20f) {
				_holdedObjects.RemoveAt(objIdx);	
			}else {
				Vector3 pullPoint = _look.TransformPoint(0, 0, _holdedObjects[objIdx].distance);
				Vector3 forceDirection = (pullPoint + Random.insideUnitSphere) - _holdedObjects[objIdx].rb.transform.position;

				_holdedObjects[objIdx].rb.AddForce(forceDirection.normalized * pullForce * Time.fixedDeltaTime * (10/_holdedObjects[objIdx].distance) );
			}
		}		
	}

    void OnTriggerEnter(Collider other) {
		if(other.gameObject.layer == interactiveLayer){// && !_holdedObjects.Contains(other.GetComponent<Rigidbody>()) ) {
			var objRb = other.GetComponent<Rigidbody>();
			float distance = Vector3.Distance(_look.position, objRb.transform.position);
			_holdedObjects.Add(new HoldedObject(objRb, _pullPoint.position, distance));
		} 
	 }




	 void OnTriggerExit(Collider other) {
		// if(other.gameObject.layer == LayerMask.NameToLayer("Interactive")) {
		// 	_holdedObjects.Remove(other.GetComponent<Rigidbody>());
		// }
	 }

}


public class HoldedObject {
	public Vector3 pullPoint;
	public Rigidbody rb;
	public float distance;


	public HoldedObject(Rigidbody rb, Vector3 pullPoint, float distance) {
		this.pullPoint = pullPoint;
		this.rb = rb;
		this.distance = distance;
	}
}