﻿using UnityEngine;
using System.Collections;

public class PlayerAbilities : MonoBehaviour {

	public Crosshairs jumpCrosshairs;
	JumpModule jumpModule;
	AccurateTelekenesis kinesis;

	void Start () {
		jumpModule = GetComponent<JumpModule> ();
		kinesis = GetComponent<AccurateTelekenesis> ();
	}

	void Update () {
		if (Input.GetButtonDown("Fire1")) {
			if (!jumpModule.jumping
			&& jumpCrosshairs.active) {
					jumpModule.JumpTo(jumpCrosshairs.lastSelectedObject.position + jumpCrosshairs.lastSelectedObject.up*5);
			}else {
				kinesis.SetActive(true);
			}
		}else if (Input.GetButtonDown("Fire2")) {
			kinesis.ThrowObjects();	
		}else if (Input.GetButtonUp("Fire1")) {
			kinesis.SetActive(false);
		}

        float mouseWeel = Input.GetAxis("Mouse ScrollWheel");
        if (mouseWeel != 0) {
            kinesis.MoveObjects(mouseWeel * 4f);
        }



    }
	

}
