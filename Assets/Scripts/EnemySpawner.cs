﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

	public float spawnDelay = 3f;
	public float currSpawnDelay = 5f;
	

	public EnemyProjectile explosiveCubePrefab;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (currSpawnDelay <= 0) {
			EnemyProjectile newProjectile = (EnemyProjectile)Instantiate(explosiveCubePrefab, transform.position + transform.up, Quaternion.identity);
			newProjectile.StartFlyingToPlayer();
			currSpawnDelay = spawnDelay;
		}else {
			currSpawnDelay -= Time.deltaTime;
		}
	}
}
