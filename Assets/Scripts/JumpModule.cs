﻿using UnityEngine;
using System.Collections;

public class JumpModule : MonoBehaviour {

	private Vector3 jumpTarget, jumpStart;
	public bool jumping;
	private float jumpProgress = 0;
	private float jumpSpeed = 2; 


	public void JumpTo(Vector3 target) {
		jumping = true;
		jumpStart = transform.position;
		jumpTarget = target;// + other.transform.up*5f;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (jumping) {
			transform.position = Vector3.Slerp(jumpStart, jumpTarget, jumpProgress);
			jumpProgress += Time.deltaTime * jumpSpeed;
			// Time.timeScale = 2f * 1/(jumpProgress + 0.1f);
			if (1 < jumpProgress) {
				jumping = false;
				jumpProgress = 0;
				// Time.timeScale = 1;
				Time.fixedDeltaTime = 0.02f;
			}
		}
	}
}
