﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Telekenesis : MonoBehaviour {

	public GameObject _forceField;
	public Transform _pullPoint;
	private int interactiveLayer, handleLayer;
	private float pullForce = 2000f; 
	private Vector3 jumpTarget, jumpStart;
	private bool jumping;
	private float jumpProgress = 0;
	private float jumpSpeed = 2;

	public List<Rigidbody> _holdedObjects = new List<Rigidbody>();

	void Start () {
		 interactiveLayer = LayerMask.NameToLayer("Interactive");
		 handleLayer = LayerMask.NameToLayer("Handle");	
	}
	
	void Update () {
		if (Input.GetButtonDown("Fire1")) {
			_forceField.SetActive(true);
		}else if (Input.GetButtonUp("Fire1")) {
			_forceField.SetActive(false);
			_holdedObjects.Clear();
		}else if (Input.GetButtonDown("Fire2")) {
			for (int objIdx = 0; objIdx < _holdedObjects.Count; objIdx++) {
				// Vector3 forceDirection = _pullPoint.position - _holdedObjects[objIdx].transform.position;
				_holdedObjects[objIdx].AddForce(transform.forward * 25f, ForceMode.Impulse);
			}
		}


		if (jumping) {
			transform.position = Vector3.Slerp(jumpStart, jumpTarget, jumpProgress);
			jumpProgress += Time.deltaTime * jumpSpeed;
			// Time.timeScale = 2f * 1/(jumpProgress + 0.1f);
			if (1 < jumpProgress) {
				jumping = false;
				jumpProgress = 0;
				Time.timeScale = 1;
				Time.fixedDeltaTime = 0.02f;
			}
		}
	}

	void FixedUpdate () {
		for (int objIdx = 0; objIdx < _holdedObjects.Count; objIdx++) {
			
			if (Vector3.Distance(_holdedObjects[objIdx].transform.position, _pullPoint.position) > 5f) {
				_holdedObjects.RemoveAt(objIdx);	
			}else {
				// print(_holdedObjects.Count);
				Vector3 forceDirection = (_pullPoint.position + Random.insideUnitSphere) - _holdedObjects[objIdx].transform.position;

				_holdedObjects[objIdx].AddForce(forceDirection.normalized * pullForce * Time.fixedDeltaTime);
				// _holdedObjects[objIdx].AddForce(Vector3.up*50, ForceMode.Impulse);

			}
		}		
	}

    void OnTriggerEnter(Collider other) {
		if(other.gameObject.layer == interactiveLayer && !_holdedObjects.Contains(other.GetComponent<Rigidbody>()) ) {
			var objRb = other.GetComponent<Rigidbody>();
			EnemyProjectile eProj = other.GetComponent<EnemyProjectile>();
			if(eProj != null) {
				eProj.StopJumpingToPlayer();
			}
			_holdedObjects.Add(objRb);


			// Vector3 forceDirection = _pullPoint.position - objRb.transform.position;
			// objRb.AddForce(forceDirection.normalized * 10f, ForceMode.Impulse);
		}else if (other.gameObject.layer == handleLayer && _holdedObjects.Count == 0) {
			jumping = true;
			jumpStart = transform.position;
			jumpTarget = other.transform.position + other.transform.up*5f;
			Time.timeScale *= 4; 
		}
	 }




	 void OnTriggerExit(Collider other) {
		// if(other.gameObject.layer == LayerMask.NameToLayer("Interactive")) {
		// 	_holdedObjects.Remove(other.GetComponent<Rigidbody>());
		// }
	 }

}
