﻿using UnityEngine;
using System.Collections;

public class EnemyProjectile : MonoBehaviour {

	public Vector3 currTarget;
	Vector3 startJumpPos;
	Vector3 _targetJumpPos;
	float _jumpProgress;
	float _jumpSpeed = 1f;
	bool _jumping;
	Rigidbody rb;

	public void StartFlyingToPlayer () {
			
			startJumpPos = transform.position;
			_targetJumpPos = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>().position;
			_jumping = true;
	}

	public void StopJumpingToPlayer () {
			_jumping = false;
			_jumpProgress = 0;
	}

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
	}


	
	
	// Update is called once per frame
	void Update () {
		if (_jumping) {
			currTarget = Vector3.Slerp(startJumpPos, _targetJumpPos, _jumpProgress);
			rb.MovePosition(currTarget);
			
			// transform.position = currTarget; 
			_jumpProgress += Time.deltaTime * _jumpSpeed;
			if (1 < _jumpProgress) {
				_jumping = false;
				_jumpProgress = 0;
			}
		}
	}

    void OnCollisionEnter(Collision other) {

        
		_jumping = false;


		// float radius = 10f;
		// float force = 500f;

		// Vector3 explosionPos = transform.position;
        // Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        // foreach (Collider hit in colliders) {
        //     Rigidbody rb = hit.GetComponent<Rigidbody>();
            
        //     if (rb != null)
        //         rb.AddExplosionForce(force, explosionPos, radius, 3.0F);
            
        // }

        
        Destroy(gameObject, 2f);
    }
}
