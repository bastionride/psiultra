﻿using UnityEngine;
using System.Collections;

public class EnemyPart : MonoBehaviour {


	public delegate void EnemyPartAction();
	public event EnemyPartAction OnRecievedDamage;

	
	void Start () {
        GetComponentInParent<Enemy>().OnDeath += OnDeath;

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnDeath() {
        gameObject.layer = LayerMask.NameToLayer("Interactive");
        Destroy(this);
    }

	void OnCollisionEnter(Collision other) {
		OnRecievedDamage();
	}

}
