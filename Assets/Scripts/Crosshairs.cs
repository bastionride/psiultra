﻿using UnityEngine;
using System.Collections;

public class Crosshairs : MonoBehaviour {


	 AccurateTelekenesis kenesisModule;
	// public float center_x, center_y, center_z, extends_x, extends_y, extends_z;
	private int handleLayer;
 	public Texture2D crosshairTexturePassive, crosshairTextureActive;
	public bool active;
     public float crosshairScale = 1;
	 public Transform lastSelectedObject;

	// Use this for initialization
	void Start () {
		kenesisModule = GetComponent<AccurateTelekenesis> ();
		handleLayer = LayerMask.NameToLayer("Handle");

	}
	
	// Update is called once per frame
	void Update () {
		
		RaycastHit hitInfo;
		if (Physics.Raycast(transform.position, transform.forward, out hitInfo, 30f)) {
			if(hitInfo.collider.gameObject.tag == "Handle") {
				active = true;
				lastSelectedObject = hitInfo.collider.gameObject.transform;
			}else {
				active = false;
			}
		}

		Debug.DrawRay(transform.position, transform.forward*30f, Color.green);


		//Doesn't fucking work
		
		// ExtDebug.DrawBoxCastBox(transform.position, new Vector3(extends_x, extends_y, extends_z), Quaternion.identity, transform.forward, 30f, Color.red);



		// Physics.BoxCast()

		// BoxCast(Vector3 center, Vector3 halfExtents, Vector3 direction, out RaycastHit hitInfo, Quaternion orientation = Quaternion.identity, float maxDistance = Mathf.Infinity, int layerMask = DefaultRaycastLayers,


		// if (Physics.BoxCast(transform.position, new Vector3(extends_x, extends_y, extends_z), transform.forward, out hitInfo, Quaternion.identity, 30f, 9)) {
		// 	print(hitInfo.collider.gameObject.name);
		// }

		
		
	}


	
     void OnGUI()
     {
         //if not paused
         if(Time.timeScale != 0)
         {
             if(crosshairTexturePassive!=null)
                         GUI.DrawTexture(new Rect((Screen.width-crosshairTexturePassive.width*crosshairScale)/2,
						 					(Screen.height-crosshairTexturePassive.height*crosshairScale)/2,
											 crosshairTexturePassive.width*crosshairScale,
											 crosshairTexturePassive.height*crosshairScale),
											 active ? crosshairTextureActive : crosshairTexturePassive);
             else
                 Debug.Log("No crosshair texture set in the Inspector");
         }
     }
}
