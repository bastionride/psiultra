﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	public EnemyPart[] bodyParts;
    public Transform dynamicGeometry;
    private float hp = 10f;

	NavMeshAgent navAgent;
	Transform playerTransform;

    public delegate void EnemyAction();
    public event EnemyAction OnDeath;

    // Use this for initialization
    void Start () {
		navAgent = GetComponent<NavMeshAgent> ();
		playerTransform = GameObject.FindGameObjectWithTag("Player").transform;

		for (int partIdx = 0; partIdx < bodyParts.Length; partIdx++) {
			bodyParts[partIdx].OnRecievedDamage += RecievedDamage;
		}
	}
	
	// Update is called once per frame
	void Update () {
		navAgent.destination = playerTransform.position;
	}

	void RecievedDamage () {
        hp -= 5;

        

        if (hp <= 0) {
            for (int partIdx = 0; partIdx < bodyParts.Length; partIdx++) {
                OnDeath();
			    bodyParts[partIdx].transform.SetParent(dynamicGeometry);
			    bodyParts[partIdx].GetComponent<Rigidbody>().isKinematic = false;
                Destroy(gameObject);
		    }
        }else {
            navAgent.speed *= 2;
        }
	}
}
